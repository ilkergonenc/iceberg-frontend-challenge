import axios from 'axios'

const postcodesIO = 'https:/api.postcodes.io/postcodes'
const mapquest = 'http://www.mapquestapi.com/directions/v2/routematrix?key=OTG1b1siUg8LlrMY2HqiCKn1EEol6byT'

export default {
  getCordinates(postcode) {
    return axios.get(`${postcodesIO}/${postcode}`)
      .then(res => { 
        return {
          lat: res.data.result.latitude,
          lng: res.data.result.longitude
        }
      })
      .catch(e => { console.error(e) })
  },
  getPostcode(lon, lat) {
    return axios.get(`${postcodesIO}?lon=${lon}&lat=${lat}`)
    .then(res => { 
      console.log(res.data.result[0].postcode)
      return res.data.result[0].postcode
    })
    .catch(e => { console.error(e) })
  },
  getDistanceAndTime(locations) {
    return axios.post(mapquest,
    {
      locations: locations,
      options: {
        manyToOne: true
      }
    }).then(res => { 
      function secondsToHms(d) {
        d = Number(d)
        let h = Math.floor(d / 3600)
        let m = Math.floor(d % 3600 / 60)
        let s = Math.floor(d % 3600 % 60)
    
        let hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : ""
        let mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : ""
        let sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : ""
        return hDisplay + mDisplay + sDisplay
      }
      function milesToKm(m) {
        return (m *  1.60934) + ' km'
      }
      let distance = milesToKm(res.data.distance[1])
      let time = secondsToHms(res.data.time[1])
      let hour = Math.floor(res.data.time[1] / 3600)
      let min = Math.floor(res.data.time[1] % 3600 / 60)
      // console.log({ distance: distance, time: time }) 
      return {
        distance: distance,
        time: time,
        hourAndMin: {
          hours: hour,
          minutes: min
        }
      }
    }).catch(e => { console.error(e) })
  }
}